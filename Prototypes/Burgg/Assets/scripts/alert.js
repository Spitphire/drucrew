﻿#pragma strict

import UnityEngine.UI;


//var goSprite : Sprite;
//var stopSprite : Sprite;
var goMaterial : Material;
var stopMaterial : Material;
var startingLives : int;
var livesText : Text;

private var currentLives : int;


var myAlert : int;


function Start ()
{
	currentLives = startingLives;
	livesText.text = "Lives: ";
	pickRandomAlert ();
}

function pickRandomAlert () : IEnumerator
{
	yield WaitForSeconds (3);

	var randomNumber = Random.Range(1, 3);
	
	myAlert = randomNumber;
	
	if (randomNumber == 1)
		{
			renderer.material = goMaterial;
		
			//SpriteRenderer.sprite = goSprite;
			
		}
	
	
	else if (randomNumber == 2)
		{
			renderer.material = stopMaterial;
		
			//SpriteRenderer.sprite = stopSprite;
		
		}
		 
	pickRandomAlert ();
}

function OnTriggerEnter (other : Collider)
{
	
	
	print("something triggered");
	if (other.gameObject.CompareTag("Player"))
	{
		if (myAlert == 2)
			{
				currentLives--;
		
				print ("Spotted!");
		
				livesText.text = "Lives: " + currentLives;
			}
	}

}

function Update (){
	if (currentLives <= 0)
			{
				print ("game over");
				livesText.text = "Game Over!";
			}
			else
			{
				livesText.text = "Lives: " + currentLives;
			}
}

