﻿#pragma strict


var currentScore : int;
var scoreText : UnityEngine.UI.Text;
var scrollSpeed : Vector3;

function Start () {

	currentScore = 0;
	scoreText.text = "Score: ";
}

function Update () {

{
 	transform.Translate(scrollSpeed * Time.deltaTime);
 	}
}

function OnTriggerEnter (other : Collider)
{
	print("something triggered");
	if (other.gameObject.CompareTag("Player"))
	{
		currentScore++;
		
		print ("CASH!");
		
		scoreText.text = "Score: " + currentScore;
	}

}
