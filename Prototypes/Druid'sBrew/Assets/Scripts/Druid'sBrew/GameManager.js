﻿#pragma strict

import UnityEngine.UI;

var thePrefab : GameObject;
var theRecipe : Recipe;
var scoreText : Text;
var recipeText : Text;

var recipe1Material : Material;
var recipe2Material : Material;
var recipe3Material : Material;

var myRecipe : int;
var currentScore : int;

//var ingredient : Recipe;

var trigger : Trigger;



function Start ()
{
    randomizeIngredientRequired ();
    currentScore = 0;
	scoreText.text = "Score: ";
	
}

function Update () 
{
	if (Input.GetButtonDown("Fire1"))
	{
		// the player has pressed the button!!!
		if (theRecipe.myRecipe == myRecipe && trigger.IamTriggered)
		{
			currentScore++;
			var instance : GameObject = Instantiate(thePrefab, transform.position, transform.rotation);
			randomizeIngredientRequired();
			theRecipe.goInvisible();
		}
		else
		{
			currentScore--;
		}		
        scoreText.text = "Score: " + currentScore;
      
        print ("my recipe is: " + myRecipe + "\nCurrent recipe is " + theRecipe.myRecipe);  
	}
}

function randomizeIngredientRequired ()
{
	var randomNumber = Random.Range (0, 3);
	
	switch (randomNumber )
	{
		case 0:
			myRecipe = 1;
			recipeText.text = "Yellow ";
			break;
		case 1:
			myRecipe = 2;
			recipeText.text = "Blue ";
			break;
		case 2:
			myRecipe = 3;
			recipeText.text = "Pink ";
			break;
			
		scoreText.text = "";

	}
}


