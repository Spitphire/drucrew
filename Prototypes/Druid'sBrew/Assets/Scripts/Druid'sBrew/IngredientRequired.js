﻿#pragma strict

var recipe1Material : Material;
var recipe2Material : Material;
var recipe3Material : Material;

var myRecipe : int;

function Start ()
{
	randomizeIngredientRequired ();
}

function randomizeIngredientRequired ()
{
	var randomNumber = Random.Range (0, 3);
	
	switch (randomNumber )
	{
		case 0:
			myRecipe = 1;
			renderer.material = recipe1Material;
			break;
		case 1:
			myRecipe = 2;
			renderer.material = recipe2Material;
			break;
		case 2:
		myRecipe = 3;
			renderer.material = recipe3Material;
			break;
	}
}
