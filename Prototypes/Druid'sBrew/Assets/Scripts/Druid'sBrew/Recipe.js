﻿#pragma strict

var scrollSpeed : float;

var recipe1Material : Material;
var recipe2Material : Material;
var recipe3Material : Material;
// 1 = yellow , 2 = blue, 3 = pink

var myRecipe : int;
var desigNum : int;

function Start ()
{
	randomizeRecipe ();
}


function FixedUpdate ()
{
	transform.Translate(scrollSpeed, 0, 0);
}

function Update()
{

	if (transform.position.x > 15) // time to respawn
	{
		transform.position.x = -15; // move to the left
		randomizeRecipe ();
		renderer.enabled = true;
	}
}

function randomizeRecipe ()
{
	var randomNumber = Random.Range (1, 4);
	
	myRecipe = randomNumber;
	print ("random recipe was: " + myRecipe);
	
	if (myRecipe == 1)
	 renderer.material = recipe1Material;
	else if (myRecipe == 2)
	 renderer.material = recipe2Material;
	else if (myRecipe == 3)
	 renderer.material = recipe3Material;
}

function goInvisible()
{
	renderer.enabled = false;
}