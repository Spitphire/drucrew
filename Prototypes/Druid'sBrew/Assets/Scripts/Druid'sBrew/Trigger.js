﻿#pragma strict

var IamTriggered : boolean;


function Start ()
{
	IamTriggered = false;
}

function OnTriggerEnter (other : Collider)
{
	IamTriggered = true;
}

function OnTriggerExit (other : Collider)
{
	IamTriggered = false;
}

