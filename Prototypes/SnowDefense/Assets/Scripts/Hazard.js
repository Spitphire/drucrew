﻿#pragma strict

var scrollSpeed : Vector3;

function Start () 
{
	
}

function Update () 
{
	transform.Translate(scrollSpeed * Time.deltaTime);
	
	if (transform.position.x < - 20)					// we've exited the screen on the left - so respawn on the right somewhere
	{
		transform.position.x = 200; 						// off the screen on the right
		transform.position.y = Random.Range (-9, 9);		// random height
		collider.enabled = true;						// turn the collider back on, in case the player hit this already
	}
}