﻿#pragma strict

@Tooltip ("How high the player jumps")
var jumpForce : float;


function Update () 
{
	if (Input.GetButton("Fire1"))
	{
		//print("Player holding fire");
		rigidbody.AddForce(0, jumpForce, 0);
	}
	 else
	  rigidbody.AddForce(0, -jumpForce, 0);
}
