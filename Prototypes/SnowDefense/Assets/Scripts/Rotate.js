﻿#pragma strict

var rotateSpeed : Vector3;

function Update () 
{
	transform.Rotate(rotateSpeed * Time.deltaTime);
	}