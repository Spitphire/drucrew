﻿#pragma strict


@Tooltip ("The player's initial lives")
var startingLives : int;
var scrollSpeed : Vector3;
var livesText : UnityEngine.UI.Text;

private var currentLives : int;

function Start () 
{
	currentLives = startingLives;
}

function Update ()
{
 	transform.Translate(scrollSpeed * Time.deltaTime);
 	}
function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.CompareTag("Hazard"))
	{
		print ("player hit a hazard");
		
		// turn off the collider to avoid multiple hits
		
		other.collider.enabled = false;
		
		currentLives--;
		
		
		
		if (currentLives <= 0)
		{
			print ("game over");
			livesText.text = "Game Over!";
		}
		else
		{
			livesText.text = "Durability: " + currentLives;
		}
	}
}